{
	hostname_prefix = 'freifunk-',
	site_name = 'Freifunk Ansbach',
	site_code = 'ffan',

	domain_seed = 'c13596d8cf365f10f4e6feac7bec7df89e236f497b3ff0dae104a21408d76067',

	prefix4 = '10.123.64.0/18',
	prefix6 = '2001:470:5168::/64',

	timezone = 'CET-1CEST,M3.5.0,M10.5.0/3', -- Europe/Berlin
	ntp_servers = {'2001:470:5168::8'},

	regdom = 'DE',

	wifi24 = {
		channel = 1,
		ap = {
			ssid = 'ansbach.freifunk.net',
		},
		mesh = {
			id = 'ffan-mesh',
			mcast_rate = 12000,
		},
	},

	wifi5 = {
		channel = 44,
		ap = {
			ssid = 'ansbach.freifunk.net',
		},
		mesh = {
			id = 'ffan-mesh',
			mcast_rate = 12000,
		},
	},

	mesh = {
		vxlan = true,
		batman_adv = {
			routing_algo = 'BATMAN_IV',
			gw_sel_class = 20,
		},
	},

	next_node = {
		ip4 = '10.123.64.1',
		ip6 = '2001:470:5168::1',
	},

	mesh_vpn = {
		mtu = 1394,
		fastd = {
			methods = {'salsa2012+umac'},
			groups = {
				ipvsix = {
					limit = 1,
					peers = {
						grogu = {
							key = '0e597160f56d965d0d272b5f1b20ea84baa66b35b5ece51db1521c568d809f10',
							remotes = { 'ipv4 "88.99.136.52" port 20000' },
						}, 
					},
				},
				backbone = {
					limit = 1,
					peers = {
						tschang = {
							key = '173bb613f447d6276acd28face7242cdb99acd28d8713760e8d8e08a6267b249',
							remotes = { 'ipv4 "23.88.119.209" port 10000' },
						}, 
						fuchur = {
							key = '5551e3770026aba45ce1d997d115e8ce4dbe7ee2c69b8c2eb78b84b26c85cfd8',
							remotes = { 'ipv4 "94.130.78.72" port 10000' },
						}, 
					},
				},
			},
		},
		
		bandwidth_limit = {
			enabled = false,
			egress = 200,
			ingress = 3000,
		},
	},

	autoupdater = {
		enabled = '1',
		branch = 'stable',
		branches = {
			stable = {
				name = 'stable',
				mirrors = {
                                    'http://[2001:470:5168::8]/firmware/stable/sysupgrade/',
                                },
				good_signatures = 1,
				pubkeys = {'b8350a96f05497e729588d763c5627067f2e74146ec0ed5f73e2b645c67adc96',
				},
			},
			experimental = {
				name = 'experimental',
				mirrors = {
                                    'http://[2001:470:5168::8]/firmware/experimental/sysupgrade/',
                                },
				good_signatures = 1,
				pubkeys = {'b8350a96f05497e729588d763c5627067f2e74146ec0ed5f73e2b645c67adc96',
				},
			},
		},
	},

	setup_mode = {
		skip = false,
	},

	config_mode = {
		geo_location = {
			show_altitude = false,
			osm = {
			  center = {
			    lat = 49.305650494,
			    lon = 10.572967529,
			  },
			  zoom = 12,
			},
		},
		remote_login = {
			show_password_form = true,
			min_password_length = 10,
		},
	},
}
